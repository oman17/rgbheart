/********************************************************
* @file				 ws2812b.c
* @author			 Michal Zalas
* @version			 V1.0.0
* @date				 31/03/2020
* @brief			 WS2812B RGB diode function. WS2812B_anim_shine_toggle( (sRGB) _RGB_RED, (sRGB) _RGB_GREEN,  (sRGB) _RGB_BLUE)
********************************************************/

#include "ws2812b.h"
#include <math.h>

extern DMA_HandleTypeDef _WS2812_TIM_DMA;
extern TIM_HandleTypeDef _WS2812_TIM;

/**
 * \brief           Array of 4x (or 3x) number of WS2812B (R, G, B[, W] colors)
 */
static uint8_t WS2812B_strip_colors[_WS2812_BYTES_PER_LED * _WS2812_LEDS_CNT];

/**
 * \brief           Temporary array for dual LED with extracted PWM duty cycles
 *
 * We need LED_CFG_RAW_BYTES_PER_LED bytes for PWM setup to send all bits.
 * Before we can send data for first led, we have to send reset pulse, which must be 50us long.
 * PWM frequency is 800kHz, to achieve 50us, we need to send 40 pulses with 0 duty cycle = make array size MAX(2 * LED_CFG_RAW_BYTES_PER_LED, 40)
 */
static _WS2812_TIM_TYPE WS2812B_tmp_data[_WS2812_LEDS_CNT* _WS2812_RAW_BYTES_PER_LED + 240];


static uint8_t WS2812B_fill_pwm_data(size_t ledx, _WS2812_TIM_TYPE* ptr);

/**
 * \brief           Set R,G,B color for specific LED
 * \param[in]       index: LED index in array, starting from `0`
 * \param[in]       r,g,b: Red, Green, Blue values
 * \return          `1` on success, `0` otherwise
 */
uint8_t
WS2812B_set_color(size_t index, uint8_t r, uint8_t g, uint8_t b
#if LED_CFG_USE_RGBW
, uint8_t w
#endif /* LED_CFG_USE_RGBW */
) {
    if (index < _WS2812_LEDS_CNT) {
        WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 0] = r * (_RGB_BRIGHT/255.0f);
        WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 1] =  g * (_RGB_BRIGHT/255.0f);
        WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 2] =  b * (_RGB_BRIGHT/255.0f);
#if LED_CFG_USE_RGBW
        WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 3] =  w < _RGB_BRIGHT ? w : _RGB_BRIGHT;
#endif /* LED_CFG_USE_RGBW */
        return 1;
    }
    return 0;
}

/**
 * \brief           Set R,G,B color for specific LED
 * \param[in]       index: LED index in array, starting from `0`
 * \param[in]        sRGB
 * \return          `1` on success, `0` otherwise
 */
uint8_t
WS2812B_set_scolor(size_t index, sRGB color
#if LED_CFG_USE_RGBW
, uint8_t w
#endif /* LED_CFG_USE_RGBW */
) {
    if (index < _WS2812_LEDS_CNT) {
        WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 0] = color.r * (_RGB_BRIGHT/255.0f);
        WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 1] =  color.g * (_RGB_BRIGHT/255.0f);
        WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 2] =  color.b * (_RGB_BRIGHT/255.0f);
#if LED_CFG_USE_RGBW
        WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 3] =  w * (_RGB_BRIGHT/255.0f);
#endif /* LED_CFG_USE_RGBW */
        return 1;
    }
    return 0;
}


uint8_t
WS2812B_set_color_all(uint8_t r, uint8_t g, uint8_t b
#if LED_CFG_USE_RGBW
, uint8_t w
#endif /* LED_CFG_USE_RGBW */
) {
    size_t index;
    for (index = 0; index < _WS2812_LEDS_CNT; index++) {
        WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 0] = r * (_RGB_BRIGHT/255.0f);
        WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 1] =  g * (_RGB_BRIGHT/255.0f);
        WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 2] =  b * (_RGB_BRIGHT/255.0f);
#if LED_CFG_USE_RGBW
        WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 3] =  w * (_RGB_BRIGHT/255.0f);
#endif /* LED_CFG_USE_RGBW */
    }
    return 1;
}

uint8_t
WS2812B_set_scolor_all( sRGB color
#if LED_CFG_USE_RGBW
, uint8_t w
#endif /* LED_CFG_USE_RGBW */
) {
    size_t index;
    for (index = 0; index < _WS2812_LEDS_CNT; index++) {
        WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 0] = color.r * (_RGB_BRIGHT/255.0f);
        WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 1] =  color.g * (_RGB_BRIGHT/255.0f);
        WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 2] =  color.b * (_RGB_BRIGHT/255.0f);
#if LED_CFG_USE_RGBW
        WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 3] =  w * (_RGB_BRIGHT/255.0f);
#endif /* LED_CFG_USE_RGBW */
    }
    return 1;
}

uint8_t
#if LED_CFG_USE_RGBW
WS2812B_set_color_rgbw(size_t index, uint32_t rgbw) {
#else /* LED_CFG_USE_RGBW */
WS2812B_set_color_rgb(size_t index, uint32_t rgbw) {
#endif /* !LED_CFG_USE_RGBW */
    if (index < _WS2812_LEDS_CNT) {
        WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 0] = (rgbw >> 24) & 0xFF;
        WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 1] = (rgbw >> 16) & 0xFF;
        WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 2] = (rgbw >> 8) & 0xFF;
#if LED_CFG_USE_RGBW
        WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 3] = (rgbw >> 0) & 0xFF;
#endif /* LED_CFG_USE_RGBW */
        return 1;
    }
    return 0;
}

uint8_t
#if LED_CFG_USE_RGBW
WS2812B_set_color_all_rgbw(uint32_t rgbw) {
#else /* LED_CFG_USE_RGBW */
WS2812B_set_color_all_rgb(uint32_t rgbw) {
#endif /* !LED_CFG_USE_RGBW */
    size_t index;
    for (index = 0; index < _WS2812_LEDS_CNT; index++) {
        WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 0] = (rgbw >> 24) & 0xFF;
        WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 1] = (rgbw >> 16) & 0xFF;
        WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 2] = (rgbw >> 8) & 0xFF;
#if LED_CFG_USE_RGBW
        WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 3] = (rgbw >> 0) & 0xFF;
#endif /* LED_CFG_USE_RGBW */
    }
    return 1;
}

/**
 * \brief           Start LEDs update procedure
 * \return          `1` if update done, `0` otherwise
 */
uint8_t
WS2812B_update() {
	if(HAL_DMA_GetState(&_WS2812_TIM_DMA)!=HAL_DMA_STATE_READY)
	    return 0;

	WS2812B_update_sequence();

    return 1;
}

/**
 * \brief           Update sequence function.
 */
void
WS2812B_update_sequence() {

	HAL_TIM_PWM_Stop_DMA(&_WS2812_TIM,_WS2812_TIM_CH);
	for(uint8_t i=0;i<_WS2812_LEDS_CNT ; i++)
	{
		WS2812B_fill_pwm_data(i,&WS2812B_tmp_data[i*24]);
	}
	HAL_TIM_PWM_Start_DMA(&_WS2812_TIM, _WS2812_TIM_CH,&WS2812B_tmp_data[0], sizeof(WS2812B_tmp_data)/sizeof(_WS2812_TIM_TYPE));
}

//https://en.wikipedia.org/wiki/Logistic_function
uint8_t calc_pwm(uint8_t val)
{
    val=val/2.55;
	const float k = 0.1f;
    const float x0 = 60.0f;
    uint8_t rv=300.0f / (1.0f + exp(-k * (val - x0)));
    return rv;
}

//https://en.wikipedia.org/wiki/Logistic_function , 255-max
sRGB calc_spwm(sRGB color)
{
    color.r=color.r/2.55;
    color.g=color.g/2.55;
    color.b=color.b/2.55;
	const float k = 0.1f;
    const float x0 = 60.0f;
    sRGB color_rv;
    color_rv.r=255.0f / (1.0f + exp(-k * (color.r - x0)));
    color_rv.g=255.0f / (1.0f + exp(-k * (color.g - x0)));
    color_rv.b=255.0f / (1.0f + exp(-k * (color.b - x0)));
    return color_rv;
}

/**
 * \brief           Prepares data from memory for PWM output for timer
 * \note            Memory is in format R,G,B, while PWM must be configured in G,R,B[,W]
 * \param[in]       ledx: LED index to set the color
 * \param[out]      ptr: Output array with at least LED_CFG_RAW_BYTES_PER_LED-words of memory
 */
static uint8_t
WS2812B_fill_pwm_data(size_t ledx, _WS2812_TIM_TYPE* ptr) {
    size_t i;

    if (ledx < _WS2812_LEDS_CNT) {
        for (i = 0; i < 8; i++) {
            ptr[i] =        (WS2812B_strip_colors[_WS2812_BYTES_PER_LED * ledx + 1] & (1 << (7 - i))) ? ( _WS2812_TIM_REG->ARR / 2) : (_WS2812_TIM_REG->ARR / 4);
            ptr[8 + i] =    (WS2812B_strip_colors[_WS2812_BYTES_PER_LED * ledx + 0] & (1 << (7 - i))) ? ( _WS2812_TIM_REG->ARR / 2) : (_WS2812_TIM_REG->ARR / 4);
            ptr[16 + i] =   (WS2812B_strip_colors[_WS2812_BYTES_PER_LED * ledx + 2] & (1 << (7 - i))) ? ( _WS2812_TIM_REG->ARR / 2) : (_WS2812_TIM_REG->ARR / 4);
#if LED_CFG_USE_RGBW
            ptr[24 + i] =   (WS2812B_strip_colors[_WS2812_BYTES_PER_LED * ledx + 3] & (1 << (7 - i))) ? (2 * _WS2812_TIM_REG->ARR / 3) : (_WS2812_TIM_REG->ARR / 3);
#endif /* LED_CFG_USE_RGBW */
        }
        return 1;
    }
    return 0;
}

/**
 * \brief           Circle RGB animation, add delay to call
 */
void WS2812B_anim_load()
{
	for (uint8_t i = 0; i < _WS2812_LEDS_CNT; i++) {
		WS2812B_set_color((i + 0) % _WS2812_LEDS_CNT, 0x1F, 0, 0);
		WS2812B_set_color((i + 1) % _WS2812_LEDS_CNT, 0x1F, 0, 0);
		WS2812B_set_color((i + 2) % _WS2812_LEDS_CNT, 0, 0x1F, 0);
		WS2812B_set_color((i + 3) % _WS2812_LEDS_CNT, 0, 0x1F, 0);
		WS2812B_set_color((i + 4) % _WS2812_LEDS_CNT, 0, 0, 0x1F);
		WS2812B_set_color((i + 5) % _WS2812_LEDS_CNT, 0, 0, 0x1F);
		WS2812B_set_color((i + 6) % _WS2812_LEDS_CNT, 0x1F,0x1F,0x1F);
		WS2812B_set_color((i + 7) % _WS2812_LEDS_CNT, 0x1F,0x1F,0x1F);
		WS2812B_update();
		WS2812B_set_color_all(0, 0, 0);
	}
	WS2812B_update();
}

/**
 * \brief           Circle RGB animation, change of diode colours every two tic , add delay after call.
 *\param[in]       RGB first color, RGB second color
 */
void WS2812B_anim_two_color_toggle(sRGB color1 , sRGB color2)
{
	static uint8_t order;
	order++;
	for (uint8_t i = 0; i < _WS2812_LEDS_CNT; i++)
	{
		if((order % 2 == 0 && i%2 == 0 ) || (order % 2 == 1 && i%2 == 1 ))
		{
			WS2812B_set_color(i , color1.r, color1.g, color1.b);
		}else{
			WS2812B_set_color(i , color2.r, color2.g, color2.b);
		}
	}

	WS2812B_update();
	WS2812B_set_color_all(0, 0, 0);
}

/**
 * \brief             Fill circle with color one by one, forward direction. Add delay after call.
 */
uint8_t WS2812B_anim_fill_color(sRGB color)
{
	static uint8_t cnt_led;
	WS2812B_set_color(cnt_led % _WS2812_LEDS_CNT, color.r, color.g, color.b);
	WS2812B_update();
	cnt_led++;
	if(cnt_led % _WS2812_LEDS_CNT == 0)
	{
		cnt_led=0;
		return 1;
	}
	return 0;
}


/**
 * \brief      Fill smooth color
 */
uint8_t WS2812B_anim_fill_smooth(uint8_t index ,sRGB color)
{
	static sRGB iter;

	uint8_t check=0;
	if(iter.r<color.r)
	{
		iter.r++;
		check++;
	}
	if(iter.g<color.g)
	{
		iter.g++;
		check++;
	}
	if(iter.b<color.b)
	{
		iter.b++;
		check++;
	}
	WS2812B_set_scolor(index % _WS2812_LEDS_CNT, calc_spwm(iter));
	WS2812B_update();

	if(check==0)
	{
		iter.r=0;
		iter.g=0;
		iter.b=0;
		return 1;

	}

	return 0;
}

/**
 * \brief      Fill circle with color one by one, forward direction, but smooth. Add delay after call.
 */
uint8_t WS2812B_anim_fill_smooth_all(sRGB color)
{
	static uint8_t cnt_led;

	if(WS2812B_anim_fill_smooth(cnt_led,color))
	{
		cnt_led++;
		if(cnt_led % _WS2812_LEDS_CNT == 0)
		{
			cnt_led=0;
			return 1;
		}
	}

	return 0;
}

/**
 * \brief      Fill circle with color one by one, back direction, but smooth. Add delay after call.
 */
uint8_t WS2812B_anim_fill_smooth_reverse_all(sRGB color)
{
	static uint8_t cnt_led=_WS2812_LEDS_CNT-1;

	if(WS2812B_anim_fill_smooth(cnt_led,color))
	{
		cnt_led--;
		if(cnt_led == 0)
		{
			cnt_led=_WS2812_LEDS_CNT-1;
			return 1;
		}
	}

	return 0;
}

/**
 * \brief          Fill circle with color one by one, reverse direction.  Add delay after call.
 */
uint8_t WS2812B_anim_fill_reverse_color(sRGB color)
{
	static uint8_t cnt_led=_WS2812_LEDS_CNT-1;
	WS2812B_set_color(cnt_led % _WS2812_LEDS_CNT, color.r, color.g, color.b);
	WS2812B_update();
	cnt_led--;
	if(cnt_led == 0)
	{
		cnt_led=_WS2812_LEDS_CNT-1;
		return 1;
	}
	return 0;
}


uint8_t WS2812B_anim_fill_reverse_no_color()
{
	static uint8_t cnt_led=_WS2812_LEDS_CNT+1;
	WS2812B_set_color(cnt_led-1, 0, 0, 0);
	WS2812B_update();
	cnt_led--;
	if(cnt_led == 0)
	{
		cnt_led=_WS2812_LEDS_CNT+1;
		return 1;
	}
	return 0;
}

/**
 * \brief           Fill strip with one color, go back with another and again froward third color to end.  Add delay after call.
 */
uint8_t WS2812B_anim_fill_color_3C(sRGB color1 , sRGB color2, sRGB color3)
{
	static uint8_t color;
	if(color==0)
	{
		if(WS2812B_anim_fill_color(color1))
			color++;
	}else if(color==1)
	{
		if(WS2812B_anim_fill_reverse_color(color2))
			color++;
	}else{
		if(WS2812B_anim_fill_color(color3))
		{
			color=0;
			WS2812B_update();
			return 1;
		}
	}
	WS2812B_update();
	return 0;
}

/**
 * \brief           Fill strip with one color, go back with another and again froward third color to end etc.  Add delay after call.
 */
uint8_t WS2812B_anim_fill_color_4C(sRGB color1 , sRGB color2, sRGB color3, sRGB color4)
{
	static uint8_t color;
	if(color==0)
	{
		if(WS2812B_anim_fill_smooth_all(color1))
			color++;
	}else if(color==1)
	{
		if(WS2812B_anim_fill_smooth_reverse_all(color2))
			color++;
	}else if(color==2)
	{
		if(WS2812B_anim_fill_smooth_all(color3))
			color++;
	}else{
		if(WS2812B_anim_fill_smooth_reverse_all(color4))
		{
			color=0;
			WS2812B_update();
			return 1;
		}
	}

	return 0;
}


/**
 * \brief       Fill all diodes with random color.
 */
uint8_t WS2812B_anim_fill_rand_color()
{
	static uint8_t cnt_led;
	sRGB color;
	color.r= rand()% _RGB_BRIGHT;
	color.g= rand()% _RGB_BRIGHT;
	color.b= rand()% _RGB_BRIGHT;
	WS2812B_set_color(cnt_led % _WS2812_LEDS_CNT, color.r, color.g, color.b);
	WS2812B_update();
	cnt_led++;
	if(cnt_led % _WS2812_LEDS_CNT == 0)
	{
		cnt_led=0;
		return 1;
	}
	return 0;
}

/**
 * \brief
 */

uint8_t WS2812B_anim_shine_toggle(sRGB color1 , sRGB color2, sRGB color3)
{
	static uint8_t color;
	if(color==0)
	{
		static uint8_t i=_WS2812B_RAIN_DIV;
		i--;
		WS2812B_set_color_all(0x00,0x00,0x00);
		if(i <= 1)
		{
			color++;
			i=_WS2812B_RAIN_DIV;
		}


	}else if(color==1)
	{
		static uint8_t i=_WS2812B_RAIN_DIV;
		i--;
		for(uint8_t j=0; j< _WS2812_LEDS_CNT; j++)
		{
			if(j % 3 ==0)
			{
				WS2812B_set_color(j , color1.r/i, color1.g/i ,color1.b/i);
			}
		}
		if(i <= 1)
		{
			color++;
			i=_WS2812B_RAIN_DIV;
		}


	}else if(color==2)
	{
		static uint8_t i=_WS2812B_RAIN_DIV;
		i--;
		for(uint8_t j=0; j< _WS2812_LEDS_CNT; j++)
		{
			if(j % 3 ==1)
			{
				WS2812B_set_color(j , color2.r/i, color2.g/i ,color2.b/i);
			}
		}
		if(i <= 1)
		{
			color++;
			i=_WS2812B_RAIN_DIV;
		}

	}else{
		static uint8_t i=_WS2812B_RAIN_DIV;
		i--;
		for(uint8_t j=0; j< _WS2812_LEDS_CNT; j++)
		{
			if(j % 3 ==2)
			{
				WS2812B_set_color(j , color3.r/i, color3.g/i ,color3.b/i);
			}
		}
		if(i <= 1)
		{
			color=0;
			i=_WS2812B_RAIN_DIV;
			return 1;
		}

	}
	WS2812B_update();
	return 0;
}


/**
 * \brief     Fill all diodes with color at random sequence.
 */
uint8_t WS2812B_anim_rain(sRGB color)
{
	static uint8_t Diode_fill[_WS2812_LEDS_CNT];
	static uint8_t lock , pass, first , crnt;
	if(!first)
	{
		memset(Diode_fill,_WS2812B_RAIN_DIV,_WS2812_LEDS_CNT);
		first++;
		pass=0;
		lock=254;
		crnt=0;
	}



	if(pass<_WS2812_LEDS_CNT)
	{
		if(lock!=254)
		{
			if(Diode_fill[crnt]-- == 2)
			{
				lock=254;
				pass++;
			}
			WS2812B_set_color( crnt, color.r/Diode_fill[crnt], color.g/Diode_fill[crnt] , color.b/Diode_fill[crnt]);
		}else{
			while(1)
			{
				crnt=rand()%_WS2812_LEDS_CNT;
				if(Diode_fill[crnt]==_WS2812B_RAIN_DIV)
				{
					lock=crnt;
					break;
				}
			}
		}
	}else{
		first=0;
		return 1;
	}
	WS2812B_update();
	return 0;

}

/**
 * \brief       Fill all diodes with mixed color at random sequence.
 */
uint8_t WS2812B_anim_rain_rainbow()
{
	static sRGB color;
	static uint8_t Diode_fill[_WS2812_LEDS_CNT];
	static uint8_t lock , pass, first , crnt;
	if(!first)
	{
		memset(Diode_fill,_WS2812B_RAIN_DIV,_WS2812_LEDS_CNT);
		first++;
		pass=0;
		lock=254;
		crnt=0;
		color.r= rand()% 254;
		color.g= rand()% 254;
		color.b= rand()% 254;
	}



	if(pass<_WS2812_LEDS_CNT)
	{
		if(lock!=254)
		{
			if(Diode_fill[crnt]-- == 2)
			{
				lock=254;
				pass++;
				WS2812B_set_color( crnt, color.r/Diode_fill[crnt], color.g/Diode_fill[crnt] , color.b/Diode_fill[crnt]);
				color.r= rand()% _RGB_BRIGHT;
				color.g= rand()% _RGB_BRIGHT;
				color.b= rand()% _RGB_BRIGHT;
			}else{
				WS2812B_set_color( crnt, color.r/Diode_fill[crnt], color.g/Diode_fill[crnt] , color.b/Diode_fill[crnt]);
			}

		}else{
			while(1)
			{
				crnt=rand()%_WS2812_LEDS_CNT;
				if(Diode_fill[crnt]==_WS2812B_RAIN_DIV)
				{
					lock=crnt;
					break;
				}
			}
		}
	}else{
		first=0;
		return 1;
	}
	WS2812B_update();
	return 0;

}





//fill each by one
uint8_t WS2812B_anim_hearth_fill_seq(sRGB color)
{

	static uint8_t state;
	static sRGB iter;
	uint8_t check=0;
	switch (state) {
		case 0:		//poziom led
			WS2812B_set_color_all(0, 0, 0);
			WS2812B_set_scolor(0, calc_spwm(iter));
			if(iter.r<color.r)
			{
				iter.r++;
				check++;
			}
			if(iter.g<color.g)
			{
				iter.g++;
				check++;
			}
			if(iter.b<color.b)
			{
				iter.b++;
				check++;
			}
			if(check==0)
			{
				iter.r=0;
				iter.g=0;
				iter.b=0;
				state++;
			}
			break;
		case 1:
			WS2812B_set_scolor(1, calc_spwm(iter));
			WS2812B_set_scolor(7, calc_spwm(iter));
			if(iter.r<color.r)
			{
				iter.r++;
				check++;
			}
			if(iter.g<color.g)
			{
				iter.g++;
				check++;
			}
			if(iter.b<color.b)
			{
				iter.b++;
				check++;
			}
			if(check==0)
			{
				iter.r=0;
				iter.g=0;
				iter.b=0;
				state++;
			}
			break;
		case 2:
			WS2812B_set_scolor(2, calc_spwm(iter));
			WS2812B_set_scolor(6, calc_spwm(iter));
			if(iter.r<color.r)
			{
				iter.r++;
				check++;
			}
			if(iter.g<color.g)
			{
				iter.g++;
				check++;
			}
			if(iter.b<color.b)
			{
				iter.b++;
				check++;
			}
			if(check==0)
			{
				iter.r=0;
				iter.g=0;
				iter.b=0;
				state++;
			}
			break;
		case 3:
			WS2812B_set_scolor(3, calc_spwm(iter));
			WS2812B_set_scolor(5, calc_spwm(iter));
			if(iter.r<color.r)
			{
				iter.r++;
				check++;
			}
			if(iter.g<color.g)
			{
				iter.g++;
				check++;
			}
			if(iter.b<color.b)
			{
				iter.b++;
				check++;
			}
			if(check==0)
			{
				iter.r=0;
				iter.g=0;
				iter.b=0;
				state++;
			}
			break;
		case 4:
			WS2812B_set_scolor(4, calc_spwm(iter));
			if(iter.r<color.r)
			{
				iter.r++;
				check++;
			}
			if(iter.g<color.g)
			{
				iter.g++;
				check++;
			}
			if(iter.b<color.b)
			{
				iter.b++;
				check++;
			}
			if(check==0)
			{
				iter.r=0;
				iter.g=0;
				iter.b=0;
				state=0;
				return 1;
			}
			break;
		default:
			break;
	}
	WS2812B_update();
	return 0;

}

/**
 * \brief           Fill strip with many color
 */
uint8_t WS2812B_anim_hearth_fill_seq_m(sRGB color,sRGB color2,sRGB color3,sRGB color4,sRGB color5)
{

	static uint8_t state;
	static sRGB iter;
	uint8_t check=0;
	switch (state) {
		case 0:		//poziom led
			WS2812B_set_color_all(0, 0, 0);
			WS2812B_set_scolor(0, calc_spwm(iter));
			if(iter.r<color.r)
			{
				iter.r++;
				check++;
			}
			if(iter.g<color.g)
			{
				iter.g++;
				check++;
			}
			if(iter.b<color.b)
			{
				iter.b++;
				check++;
			}
			if(check==0)
			{
				iter.r=0;
				iter.g=0;
				iter.b=0;
				state++;
			}
			break;
		case 1:
			WS2812B_set_scolor(1, calc_spwm(iter));
			WS2812B_set_scolor(7, calc_spwm(iter));
			if(iter.r<color2.r)
			{
				iter.r++;
				check++;
			}
			if(iter.g<color2.g)
			{
				iter.g++;
				check++;
			}
			if(iter.b<color2.b)
			{
				iter.b++;
				check++;
			}
			if(check==0)
			{
				iter.r=0;
				iter.g=0;
				iter.b=0;
				state++;
			}
			break;
		case 2:
			WS2812B_set_scolor(2, calc_spwm(iter));
			WS2812B_set_scolor(6, calc_spwm(iter));
			if(iter.r<color3.r)
			{
				iter.r++;
				check++;
			}
			if(iter.g<color3.g)
			{
				iter.g++;
				check++;
			}
			if(iter.b<color3.b)
			{
				iter.b++;
				check++;
			}
			if(check==0)
			{
				iter.r=0;
				iter.g=0;
				iter.b=0;
				state++;
			}
			break;
		case 3:
			WS2812B_set_scolor(3, calc_spwm(iter));
			WS2812B_set_scolor(5, calc_spwm(iter));
			if(iter.r<color4.r)
			{
				iter.r++;
				check++;
			}
			if(iter.g<color4.g)
			{
				iter.g++;
				check++;
			}
			if(iter.b<color4.b)
			{
				iter.b++;
				check++;
			}
			if(check==0)
			{
				iter.r=0;
				iter.g=0;
				iter.b=0;
				state++;
			}
			break;
		case 4:
			WS2812B_set_scolor(4, calc_spwm(iter));
			if(iter.r<color5.r)
			{
				iter.r++;
				check++;
			}
			if(iter.g<color5.g)
			{
				iter.g++;
				check++;
			}
			if(iter.b<color5.b)
			{
				iter.b++;
				check++;
			}
			if(check==0)
			{
				iter.r=0;
				iter.g=0;
				iter.b=0;
				state++;
			}
			break;
		case 5:
			if(iter.r<color5.r)
			{
				iter.r++;
				check++;
			}
			if(iter.g<color5.g)
			{
				iter.g++;
				check++;
			}
			if(iter.b<color5.b)
			{
				iter.b++;
				check++;
			}
			if(check==0)
			{
				iter.r=0;
				iter.g=0;
				iter.b=0;
				state=0;
				return 1;
			}
			break;
		default:
			break;
	}
	WS2812B_update();
	return 0;

}

uint8_t WS2812B_anim_hearth_fill_seq_cycle(sRGB color,sRGB color2,sRGB color3,sRGB color4,sRGB color5)
{
	static uint8_t lock;
	if(lock==0)
	{
		lock= WS2812B_anim_hearth_fill_seq_m( color, color2, color3, color4, color5 );
	}else{
		if(WS2812B_anim_hearth_darknes_all()==1)
		{
			lock = 0;
			return 1;
		}
	}
	return 0;
}

#define ROW_SCALE_2	3
#define ROW_SCALE_3	5
#define ROW_SCALE_4	7
#define ROW_SCALE_5	8

uint8_t WS2812B_anim_hearth_fill(sRGB color)
{
	static uint8_t row=1;
	static uint16_t j;


	j++;
	if(row<=1)
	{
		WS2812B_set_color_all(0, 0, 0);
		uint8_t check=0;
		if(j>color.r)
		{
			check++;
		}
		if(j>color.g)
		{
			check++;
		}
		if(j>color.b)
		{
			check++;
		}
		if(check==3)
		{
			row++;
		}
		WS2812B_set_color(0, j < color.r ? j : color.r,
				j < color.g ? j : color.g,
				j < color.b ? j : color.b  );


	}
	if(row<=2)
	{
		uint8_t check=0;
		if(j>color.r*ROW_SCALE_2)
		{
			check++;
		}
		if(j>color.g*ROW_SCALE_2)
		{
			check++;
		}
		if(j>color.b*ROW_SCALE_2)
		{
			check++;
		}
		if(check==3)
		{
			row++;

		}else{
			WS2812B_set_color(1, j < (color.r*ROW_SCALE_2) ? j/ROW_SCALE_2 : color.r,
					j < (color.g*ROW_SCALE_2) ? j/ROW_SCALE_2 : color.g,
					j < (color.b*ROW_SCALE_2) ? j/ROW_SCALE_2 : color.b  );
			WS2812B_set_color(7, j < (color.r*ROW_SCALE_2) ? j/ROW_SCALE_2 : color.r,
					j < (color.g*ROW_SCALE_2) ? j/ROW_SCALE_2 : color.g,
					j < (color.b*ROW_SCALE_2) ? j/ROW_SCALE_2 : color.b  );
		}
	}
	if(row<=3)
	{
		uint8_t check=0;
		if(j>color.r*ROW_SCALE_3)
		{
			check++;
		}
		if(j>color.g*ROW_SCALE_3)
		{
			check++;
		}
		if(j>color.b*ROW_SCALE_3)
		{
			check++;
		}
		if(check==ROW_SCALE_3)
		{
			row++;

			row=1;
			j=0;
			return 1; //todo: usunas
		}else{
			WS2812B_set_color(2, j < (color.r*ROW_SCALE_3) ? j/ROW_SCALE_3 : color.r,
					j < (color.g*ROW_SCALE_3) ? j/ROW_SCALE_3 : color.g,
					j < (color.b*ROW_SCALE_3) ? j/ROW_SCALE_3 : color.b  );
			WS2812B_set_color(6, j < (color.r*ROW_SCALE_3) ? j/ROW_SCALE_3 : color.r,
					j < (color.g*ROW_SCALE_3) ? j/ROW_SCALE_3 : color.g,
					j < (color.b*ROW_SCALE_3) ? j/ROW_SCALE_3 : color.b  );
		}
	}
	if(row<=4)
	{
		uint8_t check=0;
		if(j>color.r*ROW_SCALE_4)
		{
			check++;
		}
		if(j>color.g*ROW_SCALE_4)
		{
			check++;
		}
		if(j>color.b*ROW_SCALE_4)
		{
			check++;
		}
		if(check==3)
		{
			row++;
		}else{
			WS2812B_set_color(3, j < (color.r*ROW_SCALE_4) ? j/ROW_SCALE_4 : color.r,
					j < (color.g*ROW_SCALE_4) ? j/ROW_SCALE_4 : color.g,
					j < (color.b*ROW_SCALE_4) ? j/ROW_SCALE_4 : color.b  );
			WS2812B_set_color(5, j < (color.r*ROW_SCALE_4) ? j/ROW_SCALE_4 : color.r,
					j < (color.g*ROW_SCALE_4) ? j/ROW_SCALE_4 : color.g,
					j < (color.b*ROW_SCALE_4) ? j/ROW_SCALE_4 : color.b  );
		}
	}
	if(row<=5)
	{
		uint8_t check=0;
		if(j>color.r*ROW_SCALE_5)
		{
			check++;
		}
		if(j>color.g*ROW_SCALE_5)
		{
			check++;
		}
		if(j>color.b*ROW_SCALE_5)
		{
			check++;
		}
		if(check==3)
		{
			row=1;
			j=0;
			return 1;
		}else{
			WS2812B_set_color(4, j < (color.r*ROW_SCALE_5) ? j/ROW_SCALE_5 : color.r,
					j < (color.g*ROW_SCALE_5) ? j/ROW_SCALE_5 : color.g,
					j < (color.b*ROW_SCALE_5) ? j/ROW_SCALE_5 : color.b  );
		}
	}
	WS2812B_update();
	return 0;
}
uint8_t WS2812B_anim_hearth_darknes_all_color(sRGB color)
{
	static uint8_t j;
	sRGB color_temp;
	j++;
	color_temp.r=(color.r-j)<0 ? 0:color.r-j;
	color_temp.g=(color.g-j)<0 ? 0:color.g-j;
	color_temp.b=(color.b-j)<0 ? 0:color.b-j;
	WS2812B_set_color_all(color_temp.r,color_temp.g,color_temp.b);
	if(color_temp.r ==0 && color_temp.g ==0 && color_temp.b == 0)
	{
		return 1;
	}
	WS2812B_update();
	return 0;
}
uint8_t WS2812B_anim_hearth_darknes_all()
{
	uint8_t diode_dark=0;

    for (size_t index = 0; index < _WS2812_LEDS_CNT; index++) {
    	if( WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 0] >2)
    		WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 0] -=1;
    	else
    		diode_dark++;

    	if( WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 1] >2)
			WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 1] -=1;
    	else
    		diode_dark++;
    	if( WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 2] >2)
			WS2812B_strip_colors[index * _WS2812_BYTES_PER_LED + 2] -=1;
    	else
    		diode_dark++;
    }
	if(diode_dark==_WS2812_LEDS_CNT*3)
	{
		return 1;
	}
	WS2812B_update();
	return 0;
}

uint8_t WS2812B_anim_hearth_cycle(sRGB color)
{

	static uint8_t lock;
	if(lock==0)
	{
		lock=WS2812B_anim_hearth_fill(color);
		HAL_Delay(5);
	}else{
		if(WS2812B_anim_hearth_darknes_all(color)==1)
		{
			lock = 0;
			return 1;
		}
		HAL_Delay(10);
	}
	return 0;


}

sRGB WS2812_enum_to_struct(enum RGB_COLOR color)
{
	switch(color)
	{
	case _WHITE:
		return (sRGB) _RGB_WHITE;
	case _RED:
		return (sRGB) _RGB_RED;
	case _GREEN:
		return (sRGB) _RGB_GREEN;
	case _BLUE:
		return (sRGB) _RGB_BLUE;
	case _YELLOW:
		return (sRGB) _RGB_YELLOW;
	default:
		return (sRGB) _RGB_RED;

	}
}
