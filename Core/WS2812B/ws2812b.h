/********************************************************
* @file				 ws2812b.h
* @author			 Michal Zalas
* @version			 V1.0.0
* @date				 31/03/2020
* @brief			 WS2812B RGB diode function
********************************************************/

#ifndef WS2812B_WS2812B_H_
#define WS2812B_WS2812B_H_

#include "main.h"
#include "stdlib.h"
#include "string.h"
/* Define -----------------------------------------------------------*/
#define _WS2812_LEDS_CNT					8		// Number of WS2812B in a strip row

#if _WS2812_USE_RGBW
#define _WS2812_BYTES_PER_LED           4
#else /* WS2812_USE_RGBW */
#define _WS2812_BYTES_PER_LED           3
#endif /* !WS2812_USE_RGBW */

#define _WS2812_RAW_BYTES_PER_LED       (_WS2812_BYTES_PER_LED * 8)		// RGB * 8bit per color

#define _WS2812_TIM_REG		TIM2
#define _WS2812_TIM			htim2
#define _WS2812_TIM_CH		TIM_CHANNEL_1
#define _WS2812_TIM_DMA		hdma_tim2_ch1
#define _WS2812_TIM_TYPE	uint32_t


#define _RGB_BRIGHT		0x1B	//0x1C
#define _RGB_WHITE		{0xFF,	0xFF,	0xFF}
#define _RGB_RED		{0xFF,	0x00,	0x00}
#define _RGB_GREEN		{0x00,	0xFF,	0x00}
#define _RGB_BLUE		{0x00,	0x00,	0xFF}
#define _RGB_YELLOW		{0xFF,	0xFF,	0x00}

//ANIM
#define _WS2812B_RAIN_DIV	20
/* Enums -----------------------------------------------------------*/
enum RGB_COLOR{
	_WHITE,
	_RED,
	_GREEN,
	_BLUE,
	_YELLOW,
	_LAST
};
/* Structures -----------------------------------------------------------*/
typedef struct {
	int r;
	int g;
	int b;
}sRGB;
/* Function prototypes -----------------------------------------------------------*/


#if _WS2812_USE_RGBW
uint8_t     WS2812B_set_color(size_t index, uint8_t r, uint8_t g, uint8_t b, uint8_t w);
uint8_t     WS2812B_set_scolor(size_t index, sRGB color,  uint8_t w);
uint8_t     WS2812B_set_color_all(uint8_t r, uint8_t g, uint8_t b, uint8_t w);
uint8_t     WS2812B_set_color_rgbw(size_t index, uint32_t rgbw);
uint8_t     WS2812B_set_color_all_rgbw(uint32_t rgbw);
uint8_t 	WS2812B_set_scolor_all( sRGB color , uint8_t w);
#else /* LED_CFG_USE_RGBW */
uint8_t     WS2812B_set_color(size_t index, uint8_t r, uint8_t g, uint8_t b);
uint8_t     WS2812B_set_scolor(size_t index, sRGB color);
uint8_t     WS2812B_set_color_all(uint8_t r, uint8_t g, uint8_t b);
uint8_t 	WS2812B_set_scolor_all( sRGB color);
uint8_t     WS2812B_set_color_rgb(size_t index, uint32_t rgbw);
uint8_t     WS2812B_set_color_all_rgb(uint32_t rgb);
#endif /* !LED_CFG_USE_RGBW */
uint8_t WS2812B_update();
void WS2812B_update_sequence();
uint8_t calc_pwm(uint8_t val);
sRGB calc_spwm(sRGB color);

/// ANIMATION
void WS2812B_anim_load();
void WS2812B_anim_two_color_toggle(sRGB color1 , sRGB color2);
uint8_t WS2812B_anim_fill_color(sRGB color);
uint8_t WS2812B_anim_fill_smooth(uint8_t index ,sRGB color);
uint8_t WS2812B_anim_fill_smooth_all(sRGB color);
uint8_t WS2812B_anim_fill_smooth_reverse_all(sRGB color);
uint8_t WS2812B_anim_fill_reverse_color(sRGB color);
uint8_t WS2812B_anim_fill_reverse_no_color();
uint8_t WS2812B_anim_fill_color_3C(sRGB color1 , sRGB color2, sRGB color3);
uint8_t WS2812B_anim_fill_color_4C(sRGB color1 , sRGB color2, sRGB color3, sRGB color4);
uint8_t WS2812B_anim_shine_toggle(sRGB color1 , sRGB color2, sRGB color3);
uint8_t WS2812B_anim_rain(sRGB color);
uint8_t WS2812B_anim_rain_rainbow();
uint8_t WS2812B_anim_fill_rand_color();
uint8_t WS2812B_anim_hearth_fill_seq(sRGB color);
uint8_t WS2812B_anim_hearth_fill_seq_m(sRGB color,sRGB color2,sRGB color3,sRGB color4,sRGB color5);
uint8_t WS2812B_anim_hearth_fill_seq_cycle(sRGB color,sRGB color2,sRGB color3,sRGB color4,sRGB color5);
uint8_t WS2812B_anim_hearth_fill();
uint8_t WS2812B_anim_hearth_darknes_all_color(sRGB color);
uint8_t WS2812B_anim_hearth_darknes_all();
uint8_t WS2812B_anim_hearth_cycle(sRGB color);
sRGB WS2812_enum_to_struct(enum RGB_COLOR color);
#endif /* WS2812B_WS2812B_H_ */
